const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
    eventName:{
        type: String,
        required: true
    },
    eventDate: {
        type: Date,
        required: true
    },
    speaker: {
        type: String,
        required: false
    },
    description:[{
        type: String,
    }],
    objective:[{
        type: String
    }]
})

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;