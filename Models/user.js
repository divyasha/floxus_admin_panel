const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
    Name:{
        type: String,
        required: true
    },
    Email:{
        type: String,
        required: true
    },
    Password:{
        type:String,
        required:true
    },
    role:{
        type:String,
        default:"user",
        enum:['user','admin'],

    }
},
{timestamps:true});


//bcrypting password
userSchema.pre('save', async function(next){
    const salt  =await bcrypt.genSalt();
    this.Password=await bcrypt.hash(this.Password,salt);
    next();
});

//login
userSchema.statics.login = async function(Email, Password){
    const user = await this.findOne({Email});
    if (user){
  const auth = await bcrypt.compare(Password, user.Password);
  if(auth){
      return user;
  }
    }
    
    throw Error('Invalid Details');
}


const User = mongoose.model('User', userSchema);

module.exports = User;
