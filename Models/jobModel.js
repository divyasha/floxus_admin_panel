const mongoose = require('mongoose');

const jobSchema = new mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    description:[{
        type: String
    }],
    perks:[{
        type: String,
        required: true
    }],
    link:{
        type: String
    }
})

const Job = mongoose.model('Job', jobSchema);

module.exports = Job;