const mongoose = require('mongoose');
const User = require('./user');

const courseSchema = new mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    description: [{
        type: String,
        required: true
    }],
    syllabus:[{
        type: String
    }],
    objectives:[{
        type: String
    }],
    price:{
        type: Number,
        required: true
    },
    location:{
        type: String,
        required: true
    },
    duration:{
        type: String,
        required: true
    },
    startDate:{
        type:Date,
        required: false
    }
})

const Course = mongoose.model('Course', courseSchema);

module.exports = Course;