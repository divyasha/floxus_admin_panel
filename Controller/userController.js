const User = require('../Models/user');
const jwt = require('jsonwebtoken');


const createToken = (id) => {
    return jwt.sign({id}, "usersecretkey", {
        expiresIn: '30d'
    });
}

const user_signup = async(role, req, res) => {

    const{Name, Email, Password} = req.body;
    try{
        const user = await User.create({Name, Email, Password, role});
        if(user)
        {

          res.status(201);
          res.json({
              _id: user._id,
              Name: user.Name,
              Email: user.Email,
              Role: user.role,
              token: createToken(user._id)
          })
        }
    }
    catch(err)
    {
       console.log(err);
    }
}

const user_login = async(role, req, res) => {
    const {Email, Password} = req.body;

    try {
        const user = await User.login(Email, Password);
        if(user)
        {
            res.status(201);
            res.json({
                _id: user._id,
                Name: user.Name,
                Email: user.Email,
                Role: user.role
            });
        }

    }
    catch(err)
    {
        console.log(err);
    }
}
const CheckRole = roles => (req, res, next) => {
  
  if(!roles.includes(req.user.role)) {
      res.status(401).json("Unauthorized")
  }
  else{
      next();
  }
}


module.exports = {
    user_signup,
    user_login,
    CheckRole
}