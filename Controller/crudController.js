const Course = require('../Models/courseModel');
const User = require('../Models/user');

const Create = async(model, req, res) => {
    try{
        const create_model= await model.create(req.body);
        if(create_model) {
            res.status(201).json({
                Details: create_model
            })
        }
        else{
            console.log("error");
            res.json({message: "Error"});
        }
    }
    catch(err)
    {
        console.log(err);
        res.stauts(401).json({
            msg: "Server Error"
        });
    }
}

const Read = async(model, req, res) => {
    try{
        const get_model = await model.find();
        res.status(200).json({
            Details: get_model
        });
    }
    catch(err)
    {
        console.log(err);
        res.stauts(401).json({
            msg: "Server Error"
        });
    }
}

const Delete = async(model, req, res) => {
    try{
    const delete_model = await model.findByIdAndDelete({_id: req.params.id})
    if(delete_model){
        res.status(200).json({
            msg: "Item Deleted"
        })
    }
    else{
        res.json({
            msg: "Item not present"
        })
    }
}


catch(err)
{
    console.log(err);
    res.status(401).json({
        msg: "Server Error"
    });
}
}


const Update = async(model, req, res) => {
    try{
    const update_model = await model.findOneAndUpdate({_id: req.params.id}, req.body)
    res.status(201).json({
        msg: "Updated Succefully"
    });
    }
    catch(err)
    {
        console.log(err);
        res.status(401).json({
            msg: "Server Error"
        });
    }
}

module.exports = {
    Create,
    Read,
    Delete,
    Update
}