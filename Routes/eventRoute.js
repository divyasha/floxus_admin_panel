const express  = require('express');
const router = express.Router();
const Auth = require('../Middleware/auth');
const crudController = require('../Controller/crudController');
const userController = require('../Controller/userController');
const Event = require('../Models/EventModel');

router.post('/create-event', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Create(Event, req, res);
});

router.get('/get-event', Auth, userController.CheckRole(["admin", "user"]), async(req, res) => {
    await crudController.Read(Event, req, res);
});

router.put('/update-event/:id', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Update(Event, req, res);
});

router.delete('/delete-event/:id', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Delete(Event, req, res);
});


module.exports = router;