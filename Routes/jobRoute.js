const express  = require('express');
const router = express.Router();
const Auth = require('../Middleware/auth');
const crudController = require('../Controller/crudController');
const userController = require('../Controller/userController');
const Job = require('../Models/jobModel');

router.post('/create-job', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Create(Job, req, res);
});

router.get('/get-job', Auth, userController.CheckRole(["admin","user"]), async(req, res) => {
    await crudController.Read(Job, req, res);
});

router.put('/update-job/:id', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Update(Job, req, res);
});

router.delete('/delete-job/:id', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Delete(Job, req, res);
});


module.exports = router;