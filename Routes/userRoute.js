const express = require('express');
const router = express.Router();
const userController = require('../Controller/userController');
const Auth = require('../Middleware/auth');

module.exports = function(router) {
    
}
router.post('/user-signup', async(req, res) => {
    await userController.user_signup("user", req, res);
})
router.post('/user-login', async(req, res) => {
    await userController.user_login("user", req, res);
})
router.post('/admin-signup', async(req, res) => {
    await userController.user_signup("admin", req, res);
})
router.post('/admin-login', async(req, res) => {
    await userController.user_login("admin", req, res);
})


module.exports = router;

