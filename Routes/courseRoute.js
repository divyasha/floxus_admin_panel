const express  = require('express');
const router = express.Router();
const Auth = require('../Middleware/auth');
const crudController = require('../Controller/crudController');
const userController = require('../Controller/userController');
const Course = require('../Models/courseModel');

router.post('/create-course', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Create(Course, req, res);
});

router.get('/get-course', Auth, userController.CheckRole(["admin", "user"]), async(req, res) => {
    await crudController.Read(Course, req, res);
});

router.put('/update-course/:id', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Update(Course, req, res);
});

router.delete('/delete-course/:id', Auth, userController.CheckRole(["admin"]), async(req, res) => {
    await crudController.Delete(Course, req, res);
});


module.exports = router;