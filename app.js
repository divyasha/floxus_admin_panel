const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const colors = require('colors');
const connectDB = require('./Config/index');
const router = express.Router();
const app = express();

dotenv.config();

connectDB();

app.use(express.json());

//Routes
var UserRoute = require('./Routes/userRoute');
var CourseRoute = require('./Routes/courseRoute');
var EventRoute = require('./Routes/eventRoute');
var JobRoute = require('./Routes/jobRoute')


app.use('/api/v1', UserRoute);
app.use('/api/v1', CourseRoute);
app.use('/api/v1', EventRoute);
app.use('/api/v1', JobRoute)

app.get('/', (req, res) => {
  res.send('It working on my machine too!');
});

app.listen(8080, () => {
  console.log('Server started at port 8080');
});
